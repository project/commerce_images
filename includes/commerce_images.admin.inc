<?php

/**
 * @file
 * Contains code for configuring commerce_images module.
 */

/**
 * Commerce Images admin form.
 */
function commerce_images_admin_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  // Select the product display bundles that Commerce Images shoudl be added to.
  $form['commerce_images_product_display_bundles'] = [
    '#type' => 'fieldset',
    '#title' => t('Select Product Display Bundles'),
    '#description' => t('Select which product display bundles that Commerce Images should be added to.'),
  ];

  // Grab all the node types that reference a commerce product.
  $product_display_bundle_options = [];
  $commerce_product_reference_node_types = commerce_product_reference_node_types();
  foreach ($commerce_product_reference_node_types as $type => $bundle) {
    $product_display_bundle_options[$type] = $bundle->name;
  }

  // Get current default values.
  $enabled_product_display_bundles = variable_get('commerce_images_display_bundles', []);
  $enabled_product_display_images = variable_get('commerce_images_image_fields', []);
  $enabled_product_display_product_reference_fields = variable_get('commerce_images_product_reference_fields', []);

  // Now go through, and generate elements for each product display bundle.
  foreach ($product_display_bundle_options as $type => $bundle) {
    $form['commerce_images_product_display_bundles'][$type] = [
      '#type' => 'fieldset',
      '#title' => $bundle,
    ];

    $form['commerce_images_product_display_bundles'][$type]['commerce_images_display_bundle'] = [
      '#type' => 'checkbox',
      '#title' => $bundle,
      '#default_value' => isset($enabled_product_display_bundles[$type]) ? 1 : 0,
    ];

    // Select the product reference field.
    $form['commerce_images_product_display_bundles'][$type]['commerce_images_product_reference_field_for_' . $type] = [
      '#type' => 'select',
      '#title' => t('Select Product Reference Field'),
      '#options' => commerce_fetch_product_reference_fields($type),
      '#description' => t('Select the commerce product reference field to use for this product display bundle.'),
      '#default_value' => isset($enabled_product_display_product_reference_fields[$type]) ? $enabled_product_display_product_reference_fields[$type] : [],
      '#states' => [
        'visible' => [
          ':input[name="commerce_images_product_display_bundles[' . $type . '][commerce_images_display_bundle]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Select the image field of the product type.
    $form['commerce_images_product_display_bundles'][$type]['commerce_images_image_field_for_' . $type] = [
      '#type' => 'select',
      '#title' => t('Select Image Field'),
      '#options' => commerce_fetch_image_fields(),
      '#description' => t('Select which image field to use for this product display bundle.'),
      '#default_value' => isset($enabled_product_display_images[$type]) ? $enabled_product_display_images[$type] : [],
      '#states' => [
        'visible' => [
          ':input[name="commerce_images_product_display_bundles[' . $type . '][commerce_images_display_bundle]"]' => ['checked' => TRUE],
        ],
      ],
    ];
  }

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Apply Changes'),
  ];

  return $form;
}

/**
 * Validate function for Commerce Images admin form.
 */
function commerce_images_admin_form_validate($form, &$form_state) {
  foreach ($form_state['values']['commerce_images_product_display_bundles'] as $type => $bundle) {
    if ($bundle['commerce_images_display_bundle'] == 1) {
      // If a bundle is selected, ensure an image field is also selected.
      if (empty($bundle['commerce_images_image_field_for_' . $type])) {
        form_set_error('commerce_images_product_display_bundles][' . $type . '][commerce_images_image_field_for_' . $type, t('Please select an image field for the product display bundle @type.', [
          '@type' => $type,
        ]));
      }
      // If a bundle is selected, ensure the product reference field is also
      // selected.
      if (empty($bundle['commerce_images_product_reference_field_for_' . $type])) {
        form_set_error('commerce_images_product_display_bundles][' . $type . '][commerce_images_product_reference_field_for_' . $type, t('Please select a commerce product reference for the product display bundle @type.', [
          '@type' => $type,
        ]));
      }
    }
  }
}

/**
 * Submit function for Commerce Images admin form.
 */
function commerce_images_admin_form_submit($form, &$form_state) {
  $enabled_product_display_bundles = [];
  $enabled_product_display_images = [];
  foreach ($form_state['values']['commerce_images_product_display_bundles'] as $type => $bundle) {
    if ($bundle['commerce_images_display_bundle'] == 1) {
      $enabled_product_display_bundles[$type] = $type;
      $enabled_product_display_images[$type] = $bundle['commerce_images_image_field_for_' . $type];
      $enabled_product_display_product_reference_fields[$type] = $bundle['commerce_images_product_reference_field_for_' . $type];
    }
  }
  variable_set('commerce_images_display_bundles', $enabled_product_display_bundles);
  variable_set('commerce_images_image_fields', $enabled_product_display_images);
  variable_set('commerce_images_product_reference_fields', $enabled_product_display_product_reference_fields);

  drupal_set_message(t('Successfully saved changes.'));
}
