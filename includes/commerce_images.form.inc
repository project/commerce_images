<?php

/**
 * @file
 * Contains code for the commerce_images forms.
 */

/**
 * The product images form.
 */
function commerce_images_product_images_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  $node = $form_state['build_info']['args'][0];
  $form_state['products'] = commerce_images_fetch_products_in_node($node);

  // Get the image field that the products in this bundle use from the config.
  $product_display_image_fields = variable_get('commerce_images_image_fields', []);
  $form_state['selected_image_field'] = $product_display_image_fields[$node->type];

  // First check if the product variation image field selected exists for all
  // the products in this product display bundle.
  if (!commerce_images_image_configured_for_product_display($node, $form_state['products'])) {
    drupal_set_message(t('There are products in this display that do not have the image field selected in the Commerce Images configuration page.
      Please check the !configuration page to ensure you have selected the correct image field.', [
        '!configuration' => l(t('configuration'), 'admin/commerce/config/commerce-images'),
      ]
    ), 'warning');
  }

  if (!empty($form_state['products'])) {
    $images = [];
    $image_options = [];
    $selected_image_field = $form_state['selected_image_field'];
    if (!empty($selected_image_field) && !empty($form_state['products'])) {
      foreach ($form_state['products'] as $product_id => $product) {
        $product_label = commerce_images_attributes_label($product);
        if (!empty($product->$selected_image_field[LANGUAGE_NONE])) {
          foreach ($product->$selected_image_field[LANGUAGE_NONE] as $key => $value) {
            $images[$product->$selected_image_field[LANGUAGE_NONE][$key]['fid']]['file'] = $product->$selected_image_field[LANGUAGE_NONE][$key];
            $images[$product->$selected_image_field[LANGUAGE_NONE][$key]['fid']]['products'][$product_id] = $product_label;
          }
        }
        // Options for our checkboxes.
        $image_options[$product_id] = $product_label;
      }
      ksort($images);
    }
  }

  // Only display the upload fields, if we're not confirming a remove image.
  if (!isset($form_state['remove_image']) || $form_state['remove_image'] == FALSE) {
    $form['upload'] = [
      '#type' => 'fieldset',
    ];

    $form['upload']['upload_image'] = [
      '#type' => 'managed_file',
      '#title' => t('Upload a New Image'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg'],
      ],
      '#upload_location' => 'public://variation_images',
      '#required' => TRUE,
    ];

    $form['upload']['alt_text'] = [
      '#type' => 'textfield',
      '#title' => t('Optional alt text'),
    ];

    $form['upload']['select_products'] = [
      '#type' => 'checkboxes',
      '#title' => t('Choose which SKUs this image represents'),
      '#options' => $image_options,
      '#required' => TRUE,
    ];

    $form['upload']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save New Image'),
    ];
  }

  // The manage existing images table.
  if (!empty($form_state['products'])) {
    // Add the form elements to the table.
    $form['existing_images'] = [
      '#type' => 'fieldset',
      '#title' => t('Manage Existing Images'),
    ];

    foreach ($images as $fid => $image_info) {
      // If we're confirming a remove image, only display the fields for that
      // particular fid.
      if (isset($form_state['remove_image']) && $form_state['remove_image'] == TRUE) {
        if ($fid != $form_state['triggering_element']['#fid']) {
          continue;
        }
      }

      $image = [
        'style_name' => 'medium',
        'path' => $image_info['file']['uri'],
        'width' => '',
        'height' => '',
        'alt' => $image_info['file']['alt'],
        'title' => $image_info['file']['title'],
      ];

      $form['existing_images'][$fid]['image'] = [
        '#markup' => theme('image_style', $image),
      ];

      $form['existing_images'][$fid]['products'] = [
        '#type' => 'checkboxes',
        '#title' => '',
        '#options' => $image_options,
        '#default_value' => isset($form_state['values']['existing_images'][$fid]['products']) ? $form_state['values']['existing_images'][$fid]['products'] : array_keys($images[$fid]['products']),
      ];

      $form['existing_images'][$fid]['remove_button'] = [
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#submit' => ['commerce_images_product_image_remove_submit'],
        '#limit_validation_errors' => [['existing_images']],
        '#name' => 'remove-image-button-' . $fid,
        '#fid' => $fid,
        '#element_key' => 'remove-button',
        '#access' => (!isset($form_state['triggering_element']) || $form_state['triggering_element']['#element_key'] == 'remove-button') ? TRUE : FALSE,
      ];

      $form['existing_images'][$fid]['save_button'] = [
        '#type' => 'submit',
        '#value' => t('Save Changes'),
        '#submit' => ['commerce_images_product_image_save_submit'],
        '#limit_validation_errors' => [['existing_images']],
        '#name' => 'save-changes-button-' . $fid,
        '#fid' => $fid,
        '#element_key' => 'save-changes-button',
        '#access' => (!isset($form_state['triggering_element']) || $form_state['triggering_element']['#element_key'] == 'save-changes-button') ? TRUE : FALSE,
      ];
    }
  }

  // If the remove button was clicked, show the confirmation checkbox.
  if (isset($form_state['remove_image']) && $form_state['remove_image'] == TRUE) {
    drupal_set_message(t('You must click the confirmation checkbox to confirm that you want to delete the image from the listed SKUs.'), 'warning');

    $form['existing_images'][$form_state['triggering_element']['#fid']]['delete_confirmation'] = [
      '#type' => 'checkbox',
      '#title' => t('I understand that the image will be removed from the following SKUs: @products.', [
        '@products' => $form_state['delete_image_from_products'],
      ]),
    ];

    $form['cancel'] = [
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#element_key' => 'cancel-button',
    ];
  }

  // Theme the table.
  $form['existing_images']['#theme'] = 'commerce_images_existing_images_table';

  return $form;
}

/**
 * Submit handler for the commerce produce images form.
 */
function commerce_images_product_images_form_submit($form, &$form_state) {
  $node = $form_state['build_info']['args'][0];

  // If the cancel button was clicked, go back to the product images page.
  if ($form_state['triggering_element']['#element_key'] == 'cancel-button') {
    drupal_goto('node/' . $node->nid . '/product-images');
  }

  $selected_image_field = $form_state['selected_image_field'];
  $field = field_info_field($selected_image_field);

  // Save the file.
  $file = file_load($form_state['values']['upload']['upload_image']);
  $file->status = FILE_STATUS_PERMANENT;
  $file->alt = isset($form_state['values']['upload']['alt_text']) ? $form_state['values']['upload']['alt_text'] : '';
  file_save($file);
  file_usage_add($file, 'commerce_images', $node->type, $node->nid);

  // Now, for all the products that were checked, add the newly created image to
  // their image field.
  $products = [];
  foreach ($form_state['values']['upload']['select_products'] as $product_id => $value) {
    if ($value) {
      $product_wrapper = entity_metadata_wrapper('commerce_product', $form_state['products'][$product_id]);
      if (isset($product_wrapper->$selected_image_field)) {
        if ($field['cardinality'] == 1) {
          $product_wrapper->$selected_image_field->file->set($file);
          $product_wrapper->$selected_image_field->alt->set($file->alt);
        }
        else {
          $file = (array) $file;
          $product_wrapper->$selected_image_field[NULL]->set($file);
        }

        $product_wrapper->save();

        $products[] = commerce_images_attributes_label($form_state['products'][$product_id]);
      }
      // Else, if the image field is not present in the product, break out of
      // there and display an error message.
      else {
        drupal_set_message(t('There are products in this display that do not have the image field selected in the Commerce Images configuration page.
          Please check the !configuration page to ensure you have selected the correct image field.', [
            '!configuration' => l(t('configuration'), 'admin/commerce/config/commerce-images'),
          ]
        ), 'error');
        break;
      }
    }
  }

  drupal_set_message(t('Successfully added the new image to the following Product(s): @products.', [
    '@products' => implode(', ', $products),
  ]));
}

/**
 * Submit function for the remove button.
 */
function commerce_images_product_image_remove_submit($form, &$form_state) {
  // Get the fid to remove from the triggering element.
  $fid = $form_state['triggering_element']['#fid'];
  $selected_image_field = $form_state['selected_image_field'];
  $field = field_info_field($selected_image_field);

  // First make the user confirm before deleting.
  if (empty($form_state['values']['existing_images'][$fid]['delete_confirmation'])) {
    $products_to_be_deleted = [];
    foreach ($form_state['products'] as $product_id => $product) {
      if (isset($product->$selected_image_field)) {
        foreach ($product->$selected_image_field[LANGUAGE_NONE] as $key => $value) {
          if ($product->$selected_image_field[LANGUAGE_NONE][$key]['fid'] == $fid) {
            $products_to_be_deleted[$product_id] = commerce_images_attributes_label($product);
          }
        }
      }
      // Else, if the image field is not present in the product, break out of
      // there and display an error message.
      else {
        drupal_set_message(t('There are products in this display that do not have the image field selected in the Commerce Images configuration page.
          Please check the !configuration page to ensure you have selected the correct image field.', [
            '!configuration' => l(t('configuration'), 'admin/commerce/config/commerce-images'),
          ]
        ), 'error');
        break;
      }
    }
    $form_state['delete_image_from_products'] = implode(', ', $products_to_be_deleted);

    $form_state['remove_image'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  // Once they confirm, delete the image from the products that reference it.
  else {
    foreach ($form_state['products'] as $product_id => $product) {
      $product_wrapper = entity_metadata_wrapper('commerce_product', $product);

      foreach ($product->$selected_image_field[LANGUAGE_NONE] as $key => $value) {
        if ($value['fid'] == $fid) {
          if ($field['cardinality'] == 1) {
            $product_wrapper->$selected_image_field = NULL;
          }
          else {
            $product_wrapper->$selected_image_field->offsetUnset($key);
          }

          $product_wrapper->save();
        }
      }
    }

    drupal_set_message(t('Successfully deleted image from the following Product(s): @products.', [
      '@products' => $form_state['delete_image_from_products'],
    ]));
  }
}

/**
 * Submit function for the save changes button.
 */
function commerce_images_product_image_save_submit($form, &$form_state) {
  // Get the fid to save the changes from the triggering element.
  $fid = $form_state['triggering_element']['#fid'];
  $selected_image_field = $form_state['selected_image_field'];
  $field = field_info_field($selected_image_field);

  // First check if the user is deleting any products.
  $products_to_be_deleted = [];
  foreach ($form_state['values']['existing_images'][$fid]['products'] as $product_id => $value) {
    if (!$value) {
      foreach ($form_state['products'][$product_id]->$selected_image_field[LANGUAGE_NONE] as $key => $value) {
        if ($form_state['products'][$product_id]->$selected_image_field[LANGUAGE_NONE][$key]['fid'] == $fid) {
          $products_to_be_deleted[$product_id] = commerce_images_attributes_label($form_state['products'][$product_id]);
        }
      }
    }
  }

  // If there are products to be deleted, make them confirm first.
  if (!empty($products_to_be_deleted) && empty($form_state['values']['existing_images'][$fid]['delete_confirmation'])) {
    $form_state['delete_image_from_products'] = implode(', ', $products_to_be_deleted);

    $form_state['remove_image'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  // Once they confirm, do the official updating of the products.
  else {
    // Load the file object.
    $file = file_load($fid);

    $products_added = [];
    $products_deleted = [];
    foreach ($form_state['values']['existing_images'][$fid]['products'] as $product_id => $value) {
      $product_wrapper = entity_metadata_wrapper('commerce_product', $form_state['products'][$product_id]);

      if (isset($product_wrapper->$selected_image_field)) {
        // If the product was checked for this image, add the image reference to
        // it.
        if ($value) {
          if ($field['cardinality'] == 1) {
            $product_wrapper->$selected_image_field->file->set($file);
          }
          else {
            $file = (array) $file;
            $product_wrapper->$selected_image_field[NULL]->set($file);
          }

          $products_added[] = commerce_images_attributes_label($form_state['products'][$product_id]);
        }
        // If the product was unchecked for this image and previously it was
        // checked we remove the image reference from the product.
        else {
          foreach ($form_state['products'][$product_id]->$selected_image_field[LANGUAGE_NONE] as $key => $value) {
            if ($value['fid'] == $fid) {
              if ($field['cardinality'] == 1) {
                $product_wrapper->$selected_image_field = NULL;
              }
              else {
                $product_wrapper->$selected_image_field->offsetUnset($key);
              }

              $products_deleted[$product_id] = commerce_images_attributes_label($form_state['products'][$product_id]);
            }
          }
        }

        $product_wrapper->save();
      }
      // Else, if the image field is not present in the product, break out of
      // there and display an error message.
      else {
        drupal_set_message(t('There are products in this display that do not have the image field selected in the Commerce Images configuration page.
          Please check the !configuration page to ensure you have selected the correct image field.', [
            '!configuration' => l(t('configuration'), 'admin/commerce/config/commerce-images'),
          ]
        ), 'error');
        break;
      }
    }

    drupal_set_message(t('Successfully saved the changes. @products_added @products_deleted', [
      '@products_added' => !empty($products_added) ? t('The image has been added to the following Product(s): @products_added.', [
        '@products_added' => implode(', ', $products_added),
      ]) : '',
      '@products_deleted' => !empty($products_deleted) ? t('The image has been removed from the following Product(s): @products_deleted.', [
        '@products_deleted' => implode(', ', $products_deleted),
      ]) : '',
    ]));
  }
}
