CONTENTS OF THIS FILE
---------------------

 * About Commerce Images
 * Features of the module

About Commerce Images
---------------------

An image management module for managing commerce product variation images easily. The module provides an easy-to-use interface that lets you select which images get displayed with which SKUs.

It provides a solution to the pain of having to upload and save an image for every product variation combination. For example, if an item is available in 3 different colors and 3 different sizes we have to save 9 images uniquely. If there are two angles of the product then we have to save and upload 18 uniquely named images.

Features of the module
----------------------
- Gives admins the ability to upload one image and select which attributes the image belongs
- Admins can manage the images (that are attached at the SKU-level) from a unique tab (displayed next to the edit tab on product displays) called "Product Images"
- Admins can add alt text with a SKU token so that images being displayed for a specific SKU can have an appropriate alt text
